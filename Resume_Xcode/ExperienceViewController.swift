//
//  ExperienceViewController.swift
//  Resume_Xcode
//
//  Created by Pontus Schavon on 2020-11-20.
//  Copyright © 2020 Pontus Schavon. All rights reserved.
//

import UIKit

class ExperienceViewController: UIViewController{
    
    let sections = ["Work", "Education"]
    
    
    @IBOutlet var experienceTableView: UITableView!
    var work: [Experience] = []
    var education: [Experience] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        experienceTableView.delegate = self
        experienceTableView.dataSource = self
        work.append(Experience(title: "Kalmartravet", dateFrom: "2015", dateTo: "2018", description: "Jag har mellan våren 2015 till slutet av sommaren 2018 jobbat på Kalmartravet under tävlingsdagarna med olika uppgifter i deras Tv-studio. Jag började med att vara ljudansvarig där jag hade koll på musiken och volymen på musiken. Det var olika låtar jag skulle spela i vissa lägen (5 minuter innan start osv.). Jag var även då ansvarig för att styra domarkamerorna som är 2 kameror som jag styrde med hjälp av en joystick. Den kollar domarna på efter loppen för att se om något har hänt eller om någon ska få någon anmärkning/böter eller liknande. Sedan gick jag över till producent i tv-studion där jag växlar mellan vilka bilder som visas på de interna skärmarna på travbanan. Där skulle jag även spela in alla lopp och ge direktiv till personen som filmar allt. Som producent hade jag även lite översikt på de andra stationerna i tv-studion.")
        )
        work.append(Experience(imageName: "phone", title: "Telia Wholesale", dateFrom: "2019", dateTo: "2020", description: "Jag jobbade här sommaren 2019 och sommaren 2020. Jag jobbade som en kundtjänst mot operatörer när de gällde fel eller åtgärder om fiber/kopparnät/adsl som operatören själv inte kan eller har behörighet att lösa."))
        education.append(Experience(imageName: "book", title: "Lindsdalsskolan", dateFrom: "2006", dateTo: "2015", description: "Jag gick ut grundskolan på Lindsdalsskolan i Lindsdal, Kalmar"))
        education.append(Experience(imageName: "book", title: "Lars Kaggskolan", dateFrom: "2015", dateTo: "2018", description: "Jag gick ut gymnasiet på Lars Kaggskolan på linjen Teknik med inriktning Informationsteknik."))
        education.append(Experience(imageName: "book", title: "Jönköping University", dateFrom: "2018", dateTo: "Nuvarande", description: "Jag går nuvarande Högskoleingenjör i Datateknik med inriktning Mjukvaruutveckling och Mobila plattformar på JTH, Jönköping University"))
    
        experienceTableView.reloadData()
    }
        
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "DetailedExperienceView" {
            let destinationViewController = segue.destination as! ExperienceDetailViewController
            destinationViewController.experienceObj = sender as? Experience
        }
    }
    
}

extension ExperienceViewController: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return self.work.count
        case 1:
            return self.education.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let headers: [String] = ["work", "Education"]
        return headers[section]
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "ExperienceCell", for: indexPath) as? ExperienceTableViewCell{
            var experienceItem = Experience()
            if indexPath.section == 0 {
                experienceItem = self.work[indexPath.row]
            }else if indexPath.section == 1 {
                experienceItem = self.education[indexPath.row]
            }
            cell.experienceImage.image = UIImage(systemName: experienceItem.image)
            cell.experienceTitle.text = experienceItem.title
            cell.experienceDate.text = experienceItem.getDate()
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var selectedCell: Experience = Experience()
        if indexPath.section == 0 {
            selectedCell = work[indexPath.row]
        }else if indexPath.section == 1 {
            selectedCell = education[indexPath.row]
        }
        performSegue(withIdentifier: "DetailedExperienceView", sender: selectedCell)
    }
    
}
