//
//  Experience.swift
//  Resume_Xcode
//
//  Created by Pontus Schavon on 2020-11-24.
//  Copyright © 2020 Pontus Schavon. All rights reserved.
//

import Foundation

class Experience {
    let image: String
    let title: String
    let dateFrom: String
    let dateTo: String
    let description: String
    
    init(imageName: String = "trash", title: String = "work", dateFrom: String = "1999", dateTo: String = "Current", description: String = "I have worked here.") {
        self.image = imageName
        self.title = title
        self.dateFrom = dateFrom
        self.dateTo = dateTo
        self.description = description
    }
    
    func getDate() -> String {
        return self.dateFrom + " - " + self.dateTo
    }
}
