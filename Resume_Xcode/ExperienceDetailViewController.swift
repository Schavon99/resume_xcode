//
//  ExperienceDetailViewController.swift
//  Resume_Xcode
//
//  Created by Pontus Schavon on 2020-12-01.
//  Copyright © 2020 Pontus Schavon. All rights reserved.
//

import UIKit

class ExperienceDetailViewController: UIViewController {
    @IBOutlet weak var navigationTitle: UINavigationItem!
    @IBOutlet weak var detailExperienceImage: UIImageView!
    @IBOutlet weak var detailHeader: UILabel!
    @IBOutlet weak var detailDate: UILabel!
    @IBOutlet weak var detailExperienceDescription: UILabel!
    
    
    var experienceObj: Experience?
    override func viewDidLoad() {
        super.viewDidLoad()
        detailExperienceImage.image = UIImage(systemName: experienceObj?.image ?? "trash")
        navigationTitle.title = experienceObj?.title
        detailHeader.text = experienceObj?.title
        detailDate.text = experienceObj?.getDate()
        detailExperienceDescription.text = experienceObj?.description
    }
}
