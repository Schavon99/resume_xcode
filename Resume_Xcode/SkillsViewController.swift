//
//  SkillsViewController.swift
//  Resume_Xcode
//
//  Created by Pontus Schavon on 2020-11-20.
//  Copyright © 2020 Pontus Schavon. All rights reserved.
//

import UIKit

class SkillsViewController: UIViewController{
    
    @IBAction func exitButtonClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBOutlet weak var skillsText: UILabel!
    
    override func viewWillAppear(_ animated: Bool) {
        UIView.animate(withDuration: 5, delay: 0, options: .curveEaseInOut, animations: {
                self.skillsText.alpha = 1
            
        }, completion: nil)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}
