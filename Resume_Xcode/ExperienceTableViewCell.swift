//
//  ExperienceTableViewCell.swift
//  Resume_Xcode
//
//  Created by Pontus Schavon on 2020-12-01.
//  Copyright © 2020 Pontus Schavon. All rights reserved.
//

import UIKit

class ExperienceTableViewCell: UITableViewCell {
    
    @IBOutlet weak var experienceImage: UIImageView!
    @IBOutlet weak var experienceTitle: UILabel!
    @IBOutlet weak var experienceDate: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
